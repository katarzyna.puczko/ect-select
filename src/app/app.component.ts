import { Component } from '@angular/core';
import { EctSelectConfig } from './ect-select/ect-select.component';

interface Typ {
  name: string;
  age: number;
  isOK: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ect-select';
  config: EctSelectConfig<Typ> = {
    label: 'Some label',
    sortDataFn: (a, b) => a.name > b.name ? 1 : a.name === b.name ? 0 : -1,
    groupDataFn: (a) => a.name[0],
    data: [{
      name: 'OptionA',
      age: 12,
      isOK: true,
    }, {
      name: 'optionB',
      age: 2,
      isOK: true,
    }, {
      name: 'optionC',
      age: 34,
      isOK: false,
    }, {
      name: 'optionY',
      age: 44,
      isOK: true,
    }],
    displayFn: (a) => a.name,
  };
}
