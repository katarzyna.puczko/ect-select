import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, QueryList } from '@angular/core';
import { NgControl } from '@angular/forms';
import { ErrorStateMatcher, MatOptgroup, MatOption, MatOptionSelectionChange } from '@angular/material/core';
import { MatSelectConfig, MatSelectTrigger } from '@angular/material/select';
import { Observable, Subject } from 'rxjs';

type GroupedData<T> = Array<{ label?: string, items: Array<T> }>;

export interface EctSelectConfig<T> {
  label?: string;
  ariaLabel?: string;
  ariaLabelledby?: string;
  compareWith?: (o1: any, o2: any) => boolean;
  disableOptionCentering?: boolean;
  disableRipple?: boolean;
  disabled?: boolean;
  errorStateMatcher?: ErrorStateMatcher;
  id?: string;
  multiple?: boolean;
  panelClass?: string | string[] | Set<string> | { [key: string]: any; };
  placeholder?: string;
  required?: boolean;
  sortComparator?: (a: MatOption, b: MatOption, options: MatOption[]) => number;
  typeaheadDebounceInterval?: number;
  value?: any;
  openedChange?: EventEmitter<boolean>;
  selectionChange?: EventEmitter<any>;
  initChange?: EventEmitter<void>;

  data: Array<T>;
  displayFn: (a: T) => string;
  sortDataFn?: (a: T, b: T) => number;
  filterDataFn?: (a: T) => boolean;
  groupDataFn?: (a: T) => string;
}

@Component({
  selector: 'ect-select',
  templateUrl: './ect-select.component.html',
  styleUrls: [ './ect-select.component.scss' ]
})
export class EctSelect<T> implements OnInit, OnChanges {
  @Input() config: EctSelectConfig<T>;
  data: GroupedData<T>;
  /** Comparison function to specify which option is displayed. Defaults to object equality. */
  private compareWith = (o1: any, o2: any) => o1 === o2;

  ngOnInit(): void {
    this.config.initChange?.emit();
  }

  ngOnChanges(): void {
    let items = this.config.data;
    if (this.config.filterDataFn) {
      items = items.filter(this.config.filterDataFn);
    }
    if (this.config.sortDataFn) {
      items = items.sort(this.config.sortDataFn);
    }
    this.data = [ { items } ];
    if (this.config.groupDataFn) {
      const groups: { [label: string]: Array<T> } = {};
      this.config.data.forEach((item) => {
        const label = this.config.groupDataFn(item);
        if (!(label in groups)) {
          groups[label] = [];
        }
        groups[label] = groups[label].concat(item);
      });
      this.data = Object.keys(groups).map((label) => ({ label, items: groups[label] }));
    }
    this.data = this.data.filter((group) => group.items.length > 0);
    console.log(this.config);
  }

}
